var app = angular.module('app', ['ui.router', 'ipCookie', 'ng-token-auth', 'templates', 'restangular', 'ngFileUpload']).config([
    '$stateProvider',
    '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('sign_in', {
                url: '/sign_in',
                templateUrl: '/angular_views/sign_in.html',
                controller: 'UserAuthController'
            })

            .state('sign_up', {
                url: '/sign_up',
                templateUrl: '/angular_views/sign_up.html',
                controller: 'UserAuthController'
            })

            .state('dashboard', {
                url: '/dashboard',
                templateUrl: '/angular_views/dashboard.html',
                controller: 'DashboardController',
                resolve: {
                    auth: function ($auth) {
                        return $auth.validateUser().catch(function(){
                            location.href = '/#/sign_in'
                        })
                    }
                }
            })

            .state('show_ticket', {
                url: '/dashboard/show_ticket/:id',
                templateUrl: '/angular_views/show_ticket.html',
                controller: 'TicketController'
            })

            .state('users', {
                url: '/dashboard/users',
                templateUrl: '/angular_views/users.html',
                controller: 'UsersController'
            })

        $urlRouterProvider.otherwise('sign_in');
    }
])

app.config(function($authProvider) {
    $authProvider.configure({
        apiUrl: BASE_URL,
        validateOnPageLoad: false
    });
})

app.config(function(RestangularProvider) {
  RestangularProvider.setBaseUrl(BASE_URL);
  RestangularProvider.setFullResponse(true);

  // Error Handler
  RestangularProvider.setErrorInterceptor(function(response, deferred, responseHandler) {
    if(response.status != 200) {
        var errors = []
        if (response.data.errors) {
            if (response.data.errors.full_messages) {
                errors = errors.concat(response.data.errors.full_messages) // For multiple errors
            } else {
                errors = errors.concat(response.data.errors)
            }
        }
    
        jQuery.gritter.add({
            title: response.statusText,
            text: errors.join("\n<br />"),
            class_name: 'response_error',
            image: '/assets/close.png',
            sticky: false,
            time: 4000
        });
        return false
    }
    return true
    });
});

// On Page load
app.run(function($location, ipCookie){
    if($location.$$path != '/sign_in' && ipCookie('currentUserId') == undefined){
        window.location.href = '/#/sign_in'
    }
});

// Get and Set Cookies
app.factory('getSetCookiesFactory', function ($rootScope, ipCookie) {
    return { getSetCookies: function () {
        $rootScope.currentUserId = ipCookie('currentUserId')
        $rootScope.currentUserRole = ipCookie('currentUserRole')
        $rootScope.currentUserName = ipCookie('currentUserName')
    }}
})


// Use for Bootstrap Nav Tabs
app.factory('navTabsFactory', function ($rootScope) {
    return { disableDefault: function () {
        angular.element(document).ready(function(){
            $('.nav-tabs li a').on('click', function(e){
                e.preventDefault();
                $(this).tab('show');
            });
        })
    }}
})

// Notification Creator
app.factory('gritterFactory', function(){
    return {notify: function(title, message){
        var unique_id = jQuery.gritter.add({
            title: title,
            text: message,
            image: '/assets/close.png',
            sticky: false,
            time: 2000,
            on_click: function(){
                jQuery.gritter.remove(unique_id, {
                    fade: false,
                    speed: 'fast',
                    fade_out_speed: 500,
                    time: 1000
                });
            }
        });
    }}
})

// Fixed for laodash error
_.contains = _.includes