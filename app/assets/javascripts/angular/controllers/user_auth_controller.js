app.controller('UserAuthController', function($scope, $state, $rootScope, $auth, ipCookie, gritterFactory, Restangular, Authenticate){
	'use strict';

	// Sign In
	$scope.signin_data = {
		email: '',
		password: ''
	}
	$scope.do_signin = function(){
		$auth.submitLogin($scope.signin_data).then(function(resp) {
            ipCookie('currentUserId', resp.id)
            ipCookie('currentUserRole', resp.role)
            ipCookie('currentUserName', resp.name)

            gritterFactory.notify('Success', 'Successfully logged-in!')
            $state.go('dashboard')
        })
        .catch(function(resp) {
            if(resp.errors.length > 0) {
            	gritterFactory.notify('Login failed', resp.errors.join("\n<br />"), 'error')
            }
        })
	}

	// Sign Up
	$scope.signUp = {
		name: '',
		email: '',
		password: ''
	}
	$scope.do_sign_up = function(){
		if($scope.signUp.password != $scope.signUp.password_confirmation){
			gritterFactory.notify('Error', "Password and confirmation doesn't matched")
			return false;
		}

		Authenticate.sign_up($scope.signUp)
		.then(function(res){
			gritterFactory.notify('Congratulations!', 'Registration was successful!')
			window.location.href = '/#/sign_in'
		})
	}

	// Sign Out
	$scope.do_signout = function(){
        $auth.signOut()
        .then(function(resp) {
	    	// Removing a cookies
			ipCookie.remove('currentUserId')
			ipCookie.remove('currentUserRole')
			ipCookie.remove('currentUserName')

			$rootScope.currentUserName = undefined
			$rootScope.currentUserRole = undefined
			$rootScope.currentUserId = undefined

			$state.reload()
			gritterFactory.notify('Success', 'Logged out!')
        })
        .catch(function(resp) {
          gritterFactory.notify('Error', 'Failed to sign out!')
        });
    }
})


