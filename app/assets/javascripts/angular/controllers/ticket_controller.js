app.controller('TicketController', function($scope, $auth, getSetCookiesFactory, gritterFactory, Restangular, $window, Upload, $state, Ticket, Comment){
    // app.js set & get cookies
    getSetCookiesFactory.getSetCookies()
    
    $scope.commentData = {		
		ticket_id: $state.params['id'],
		message: ''
	}

	$scope.appUrl = window.location.origin

	// Show Ticket Data for viewing & posting comment
	$scope.json_ticket = {}
	$scope.show_ticket_data = function(){
		Ticket.show($state.params['id'])
		.then(function(res){
			$scope.json_ticket = res.data
		})
	}
	$scope.show_ticket_data()
	
	// Get and show comments
	$scope.ticket_comments = {}
	$scope.show_ticket_comments = function(){
		Ticket.get_ticket_comments_and_attachments($state.params['id'])
		.then(function(res){
			$scope.ticket_comments = res.data.comments
		})
	}
	$scope.show_ticket_comments()


	// Support Taking Ticket
	$scope.takeTicket = function(id){
		Ticket.take_or_close_ticket(id, 'take')
        .then(function(res){
            gritterFactory.notify('Success', res.data.msg)
            $state.go('dashboard')
        })
	}

    // Support Closing Ticket
    $scope.closeTicket = function(id){
        Ticket.take_or_close_ticket(id, 'close')
        .then(function(res){
            gritterFactory.notify('Success', res.data.msg)
            $state.go('dashboard')
        })  	
    }

 	// Post New Comment
    $scope.comment = {
    	ticket_id: $state.params.id
    }
    $scope.postComment = function () {
        if(!$scope.comment.message){
         gritterFactory.notify('Error', 'Message is required!')
         return false
        }

        var btnPostComment = jQuery('button#post-comment');
        btnPostComment.attr('disabled', 'disabled'); // add disable attr
        
    	Upload.upload({
    		url: '/api/comments',
    		data: { comment: $scope.comment},
    		method: 'POST',
            headers: $auth.retrieveData('auth_headers'),
    	}).then(function (resp) {
    		$('#commentAttachmentFileToBeUploaded').text('')
    		$("textarea[name='message']").val('')
    		$scope.show_ticket_comments()
    		gritterFactory.notify('Success', 'Comment was posted.')

            btnPostComment.removeAttr("disabled"); // remove disable attr
    	}, function (resp) {
    		gritterFactory.notify('Error', 'Invalid attachment!')
    	}, function (evt) {
    		// var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
    	});
    };

    // Show Image Filename
    $scope.previewImageNames = function(files){
    	file_names = ''
    	$.each(files, function(k, v){
    		file_names	+= v['name'] + ', '
    	})
    	$('#commentAttachmentFileToBeUploaded').text(file_names)
    }

    $scope.setTicketId = function(id) {
    	$scope.comment.ticket_id = id
    }

})