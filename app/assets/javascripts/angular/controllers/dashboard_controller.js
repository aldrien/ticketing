app.controller('DashboardController', function($scope, $auth, ipCookie, $rootScope, getSetCookiesFactory, navTabsFactory, gritterFactory, $window, Upload, $state, Ticket, User){
    // app.js set & get cookies
    getSetCookiesFactory.getSetCookies()

    // Disable link redirection
    navTabsFactory.disableDefault()
	
    // Set Variables
    $scope.ticket = {}
    $scope.modalTitle = ''
    $scope.tickets = []
    $scope.open_tickets = []
    $scope.inprogress_tickets = []
    $scope.closed_tickets = []

    $scope.createTicket = function(){
        $scope.modalTitle = 'Create Ticket'
    }

    // Getting All Tickets group by status
	$scope.all_tickets = function(){
		Ticket.index()
		.then(function(res){
			$scope.open_tickets = res.data.open_tickets
			$scope.inprogress_tickets = res.data.in_progress_tickets
			$scope.closed_tickets = res.data.closed_tickets
		})
	}
    
    if(ipCookie('currentUserId')){
	   $scope.all_tickets()
    }

	// Creating or Updating Ticket
    // Upload Attachments
    $scope.createOrUpdateTicket = function () {
        if($scope.ticket.id){
            // Update Ticket
            set_url = '/api/tickets/' + $scope.ticket.id
            set_action = 'update'
            set_method = 'PATCH'
        }else{
            // Create Ticket
            set_url = '/api/tickets/'
            set_action = 'create'
            set_method = 'POST'
        }

        var btnCreateUpdateTicket = jQuery('button#create-ticket');
        btnCreateUpdateTicket.attr('disabled', 'disabled'); // add disable attr
        
        Upload.upload({
            url: set_url,
            data: { ticket: $scope.ticket },
            method: set_method,
            headers: $auth.retrieveData('auth_headers')
        }).then(function (resp) {
            $('#ticketAttachmentFileToBeUploaded').text('')
            jQuery('.modal').modal('hide')
            $scope.ticket = {}
            $scope.all_tickets()

            btnCreateUpdateTicket.removeAttr("disabled"); // remove disable attr

            if(set_action == 'create'){
                msg = 'Ticket was created!'
            }else{
                msg = 'Ticket was updated!'
            }
            gritterFactory.notify('Success', msg)
        }, function (resp) {

            gritterFactory.notify('Error', 'Internal server error!')
        }, function (evt) {});
    }

    // Shows Image File Names 
    $scope.previewImageNames = function(files){
        file_names = ''
        $.each(files, function(k, v){
            file_names  += v['name'] + ', '
        })
        $('#ticketAttachmentFileToBeUploaded').text(file_names)        
    }

    // When Edit button was clicked
    $scope.editTicket = function(id){
        $scope.modalTitle = 'Edit Ticket'

        Ticket.show(id)
        .then(function(res){
            $scope.ticket = res.data.ticket
        })
    }

    // Getting All Support Users
    $scope.support_users = {}
    $scope.getSupportUsers = function(){
    	User.index()
        .then(function(res){
            $scope.support_users = res.data.supports
        })
    }
    // Only for Admin
    if($rootScope.currentUserRole == 3){
        $scope.getSupportUsers();
    }


    // On-click delete button, pass ticket_id
    $scope.target_ticket_id = null
    $scope.setTicketId = function(id) {
    	$scope.target_ticket_id = id
    }

    // Deleting Ticket (ONLY ADMIN USER)
    $scope.deleteTicket = function(id){
    	Ticket.delete(id)
        .then(function(res){
    		$scope.all_tickets()
	    	jQuery('.modal').modal('hide')
            gritterFactory.notify('Success', 'Ticket was deleted!')
    	})
    }

    // Show Users
    $scope.showUsers = function(){
        $state.go('users')
    }

})