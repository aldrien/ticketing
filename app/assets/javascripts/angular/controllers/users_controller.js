app.controller('UsersController', function($scope, $auth, $rootScope, getSetCookiesFactory, gritterFactory, navTabsFactory, Restangular, User, Authenticate){	
    // app.js set & get cookies
    getSetCookiesFactory.getSetCookies()
    
    // Disable link redirection
    navTabsFactory.disableDefault()

    // Getting All Tickets group by status
    $scope.users = {}
    $scope.all_users = function(){
        User.index()
        .then(function(res){
            $scope.users = res.data
        })
    }
    // Get ONLY if Admin
    if($rootScope.currentUserRole == 3){
       $scope.all_users()
    }

    // Edit User Info
    $scope.user = {     
        name: '',
        email: '',
        password: '',
        password_confirmation: '',
        role: ''
    }
    // When Edit button was clicked
    $scope.editUser = function(id){
        User.show(id)
        .then(function(res){
            $scope.user = res.data
        })
    }

    // Update
    $scope.updateUser = function(){
        if($scope.user.password != $scope.user.password_confirmation){
            gritterFactory.notify('Error', "Password and confirmation doesn't matched")
            jQuery('input[name="password"]').focus()
            return false
        }

        // Authenticate.update($scope.user)
        User.update($scope.user.id, $scope.user)
        .then(function(res){
            $scope.all_users()
            jQuery('.modal').modal('hide')
            $scope.user = {}
            gritterFactory.notify('Success', 'User account was successfully updated.')
        })
    }

    // Only Allows Current Logged-In User
    $scope.updatePasswordForm = {
        password: '',
        password_confirmation: ''
    }
    $scope.updatePassword = function(){
        $auth.updatePassword($scope.updatePasswordForm)
        .then(function(resp) {
          gritterFactory.notify('Success', resp.data.message)
        })
        .catch(function(resp) {
          gritterFactory.notify('Error', resp.data.errors.full_messages)
        });
    }

    
    // On-click delete button, pass ticket_id
    $scope.target_user_id = null
    $scope.setUserId = function(id) {
        $scope.target_user_id = id
    }

    // Delete User
    $scope.deleteUser = function(id){
        User.delete(id)
        .then(function(res){
            $scope.all_users()
            jQuery('.modal').modal('hide')
            gritterFactory.notify('Success', 'User account was successfully deleted.')
        })
    }
})