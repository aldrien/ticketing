app.factory('Authenticate', ['Restangular', function (Restangular) {
  'use strict';
  Restangular.setFullResponse(true)
  var api = Restangular.all('auth');
  return {
	signin: function(params){
        return api.one('sign_in').post('', params)
    },

	sign_out: function(){
		return api.one('sign_out').remove()
	},

	sign_up: function(params){
        return api.post(params)
    }
  };
}]);

// User Management
app.factory('User', ['Restangular', function(Restangular){
	'use strict';
	Restangular.setFullResponse(true)
	var api = Restangular.all('api/users');
	return {
		index: function(){
			return api.get('')
		},

		show: function(id){
			return api.get(id)
		},

		update: function(id, params){
			return api.one('', id).patch(params)
		},

		delete: function(id){
			return api.one('', id).remove()
		}
	}
}]);

// Ticket Management
app.factory('Ticket', ['Restangular', function(Restangular){
	'use strict';
	Restangular.setFullResponse(true)
	var api = Restangular.all('api/tickets');
	return {
		index: function(){
			return api.get('')
		},

		show: function(id){
			return api.one('', id).get()
		},

		delete: function(id){
			return api.one('', id).remove()
		},

		get_ticket_comments_and_attachments: function(id){
			return api.one(id, 'get_ticket_comments_and_attachments').get()
		},

		take_or_close_ticket: function(id, action){
			return api.one('take_or_close_ticket').get({id: id, do_action: action})
		}
	};
}]);


// Comment Management
app.factory('Comment', ['Restangular', function(Restangular){
	'use strict';
	Restangular.setFullResponse(true)
	var api = Restangular.all('api/comments');
	return {
		create: function(comment_params){
			return api.post(comment_params)
		},

		delete: function(){
			return api.one('', id).delete()
		}
	};
}]);


// Use for Inline IF Statement
app.filter('iif', function () {
   return function(input, trueValue, falseValue) {
        return input ? trueValue : falseValue;
   };
});
