module ApplicationHelper
	def format_date_helper(date_time)
		date_time.to_time.strftime("%e %b %Y %H:%M") if date_time.present?
	end
end
