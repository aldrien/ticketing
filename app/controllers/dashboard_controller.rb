class DashboardController < ApplicationController
	before_action :authenticate_user!, only: :generate_closed_ticket_report

	# For angular view purpose
	def index
	end

	# Ticket PDF Report Generation
	def generate_closed_ticket_report
		@current_user = current_user
		@closed_ticket = Ticket.closed_ticket_last_month(current_user.id)

		@dateTimeNow = Time.new.strftime("%a, %e %b %Y %H:%M")

		@report_title = 'Last-Month-Closed-Ticket-Report'
		template = 'dashboard/ticket_report.pdf.erb'

		respond_to do |format|
			format.html
			format.pdf do
				render title:    @report_title,
					pdf:         @report_title,
					template:    template,
					layout:      'pdf_template.html',
					orientation: 'Landscape',
					footer: {
					  center:    '[page] of [topage]',
					  right:     '',
					  font_name: 'Arial',
					  font_size: 8
					}
			end
		end
	end
end
