class ApplicationController < ActionController::Base
	include DeviseTokenAuth::Concerns::SetUserByToken
	
	protect_from_forgery with: :null_session, only: Proc.new { |c| c.request.format.json? }

	before_action :configure_permitted_parameters, if: :devise_controller?

	def configure_permitted_parameters
	  devise_parameter_sanitizer.permit(:sign_in, keys: [:name, :email, :password])
	  devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :password, :password_confirmation]) 
	  devise_parameter_sanitizer.permit(:account_update, keys: [:role, :name, :email, :password, :password_confirmation, :current_password])
	end
end