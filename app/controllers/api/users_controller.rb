class Api::UsersController < ApiController
	before_action :authenticate_user!, except: :success_login
	before_action :set_resource, only: [:show, :update, :destroy]

	def index
		if user_signed_in?
			if current_user.is_admin?
				render json: {
			      admins: User.get_all_admins,
			      supports: User.get_all_supports,
			      customers: User.get_all_customers
			    }
		    else
		    	render json: {
			      supports: User.get_all_supports
			    }
	    	end
    	end
	end

	def show
		render json: @user
	end

	def update
		if current_user.is_admin?
			render json: @user.reload if @user.update_attributes(user_params)
		else
			raise Ticketing::Exception::Unauthorized
		end
	end

	def destroy
		if current_user.is_admin?
			render json: {msg: 'Successfully deleted.'} if @user.destroy
		else
			raise Ticketing::Exception::Unauthorized
		end
	end

	# Only for Devise Default Success Login Url
	def success_login
		render layout: false
	end
	private

	def user_params
		params.require(:user).permit(:id, :name, :role, :email, :password, :password_confirmation)
	end

	def set_resource
    	@user = User.find(params[:id]) unless params[:id].blank?
  	end
end