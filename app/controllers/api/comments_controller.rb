class Api::CommentsController < ApiController
	before_action :authenticate_user!
	before_action :set_resource, only: [:update, :destroy]

	def create
		comment = Comment.create!(ticket_id: params[:comment][:ticket_id], message: params[:comment][:message], user_id: current_user.id)
		unless params[:comment][:comment_attachments].nil?
			attachments = []
			params[:comment][:comment_attachments].each { |k, v| attachments << {docu: v, comment: comment} }
			comment.comment_attachments << CommentAttachment.create!(attachments)
		end

		render json:  {comment: comment}
	end

	def update
		if current_user.is_admin? || (@comment.user == current_user)
			@comment.update_attribute :message, params[:comment][:message]

			render json: {comment: @comment}
		else
			raise Ticketing::Exception::Unauthorized
		end
	end

	def destroy
		if current_user.is_admin? || (@comment.user == current_user)
			@comment.update_attribute :status, false

			render json: {msg: 'Successfully removed.'}
		else
			raise Ticketing::Exception::Unauthorized
		end
	end

	private
	def set_resource
		@comment = Comment.find(params[:id]) unless params[:id].blank?
	end
end
