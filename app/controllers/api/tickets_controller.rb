class Api::TicketsController < ApiController
	before_action :authenticate_user!
	before_action :set_resource, only: [:update, :destroy, :take_or_close_ticket]

	def index
		tickets = {}

		if current_user.is_customer?
			tickets = get_ticket_by_role_and_status('customer')
		elsif current_user.is_support?
			tickets = get_ticket_by_role_and_status('support')
		elsif current_user.is_admin?
			tickets = get_ticket_by_role_and_status('admin')
		end

		render json: tickets
	end

	def show
		render json: Ticket.show_ticket_and_attachments(params[:id])
	end	

	def create
		ticket = Ticket.create!(
		 subject: params[:ticket][:subject],
		 description: params[:ticket][:description],
		 customer_id: current_user.id,
		 status: 0,
		 support_id: nil
		)

		unless params[:ticket][:ticket_attachments].nil?
			upload_ticket_attachments(ticket, params[:ticket][:ticket_attachments])
		end

		render json: {ticket: ticket}
	end

	def update
		if current_user.is_admin? || (@ticket.customer == current_user)
			set_support_id = params[:ticket][:support_id].to_i == 0 ? nil : params[:ticket][:support_id]

			@ticket.update_attributes({
				subject: params[:ticket][:subject],
				description: params[:ticket][:description],
				status: params[:ticket][:status],
				support_id: set_support_id
			})
			# @ticket.update_attributes(ticket_params).reject{|k,v| v.blank?}
			unless params[:ticket][:ticket_attachments].nil?
				upload_ticket_attachments(@ticket, params[:ticket][:ticket_attachments])
			end

			render json: {ticket: @ticket.reload}
		else
			raise Ticketing::Exception::Unauthorized
		end
	end

	def destroy
		if current_user.is_admin?
			render json: {msg: 'Successfully deleted.'} if @ticket.destroy
		else
			raise Ticketing::Exception::Unauthorized
		end
	end

	def get_ticket_comments_and_attachments
		render json: Ticket.get_ticket_comments_and_attachments(params[:id], current_user)
	end

	# Support User Take/Close Ticket
	def take_or_close_ticket
		if params[:do_action] == 'take'
			@ticket.update_attributes({status: 1, support_id: current_user.id})
			msg = 'Ticket was taken.'
		else # close
			@ticket.update_attributes({status: 2, closed_at: Time.new})
			msg = 'Ticket was closed.'
		end
		
		render json: {msg: msg}
	end


	private
	def ticket_params
		params.require(:ticket).permit(
			Ticket.column_names.map {|x| x.to_sym }, 
			ticket_attachments_attributes: [
        		:id,
        		:ticket_id,
        		:docu_file_name,
				:docu_content_type,
				:docu_file_size
			]
        )
	end

	def set_resource
    	@ticket = Ticket.find(params[:id]) unless params[:id].blank?
  	end

	def get_ticket_by_role_and_status(role)
		case role
		when 'customer'
			tickets = current_user.customer_tickets
			set_open_tickets = tickets.open
		when 'support'
			tickets = current_user.support_tickets
			set_open_tickets = Ticket.where('support_id IS NULL OR support_id = ?', current_user.id).open
		else
			tickets = Ticket.all
			set_open_tickets = tickets.open
		end

		tickets = {
			open_tickets: iterate_tickets(set_open_tickets),
			in_progress_tickets: iterate_tickets(tickets.in_progress),
			closed_tickets: iterate_tickets(tickets.closed)
		}
	end

	def iterate_tickets(tickets)
		data = []
		tickets.each do |ticket|
			local = ticket.as_json
			local['formatted_status'] = ticket.get_status
			local['support'] = ticket.support.try(:name)
			local['customer'] = ticket.customer.try(:name)
			data << local
		end
		data	
	end

	def upload_ticket_attachments(ticket, ticket_attachments)
		attachments = []
		ticket_attachments.each do |k, attachment|
			ticket.ticket_attachments.create!(docu: attachment)
		end
	end
end
