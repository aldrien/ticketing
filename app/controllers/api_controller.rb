class ApiController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  
  rescue_from ActiveRecord::RecordNotFound, with: :handle_record_not_found
  rescue_from ActiveRecord::RecordInvalid, with: :handle_invalid_record
  rescue_from Ticketing::Exception::Unauthorized, with: :handle_unauthorized
  rescue_from Ticketing::Exception::BadRequest, with: :handle_bad_request
  rescue_from Ticketing::Exception::Forbidden, with: :handle_forbidden
  rescue_from ActionController::UnknownFormat, with: :handle_unknown_format
  rescue_from NotImplementedError, with: :handle_not_implemented

  def handle_record_not_found
    render json: { message: 'Not Found' }, status: 404
  end

  def handle_invalid_record(e)
    render json: { message: e.message, errors: e.record.errors }, status: 422
  end

  def handle_unauthorized(e)
    render json: { message: e.message }, status: :unauthorized
  end

  def handle_bad_request(e)
    render json: { message: e.message }, status: :bad_request
  end

  def handle_forbidden(e)
    render json: { message: e.message }, status: :forbidden
  end

  def handle_unknown_format(e)
    render json: { message: e.message, errors: e.inspect }, status: 415
  end

  def handle_not_implemented(e)
    render json: { message: e.message }, status: :not_implemented
  end

end