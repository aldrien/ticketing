class User < ApplicationRecord
	# For not sending confirmation email after signing up
    before_save -> { skip_confirmation! }
  
    include DeviseTokenAuth::Concerns::User
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable,
    devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

    has_many :customer_tickets, foreign_key: :customer_id, class_name: 'Ticket', dependent: :destroy
	has_many :support_tickets, foreign_key: :support_id, class_name: 'Ticket', dependent: :destroy

	validates_uniqueness_of :name, presence: true
	validates_uniqueness_of :email, presence: true, email: true
	validates_inclusion_of :role, :in => 1..3

	before_save :set_downcase_email

	scope :get_all_admins, -> { where(role: 3) }
	scope :get_all_supports, -> { where(role: 2) }
	scope :get_all_customers, -> { where(role: 1) }

	def is_admin?
		role == 3
	end

	def is_support?
		role == 2
	end

	def is_customer?
		role == 1
	end

	# set email to lower case before save
	def set_downcase_email
		self.email = email.downcase if email.present?    
	end
end
