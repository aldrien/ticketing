class Comment < ApplicationRecord
	default_scope { order(created_at: :desc) }
	belongs_to :ticket
	belongs_to :user
	has_many :comment_attachments, dependent: :destroy
	scope :active_comments, -> { where(status: true) }

	def rendered_attachments
		res = []
		comment_attachments.each { |attachment| res << { thumb: attachment.docu.url(:thumb), original: attachment.docu.url(:original) } }
		res
	end
end
