class CommentAttachment < ApplicationRecord
	belongs_to :comment
	
	has_attached_file :docu, :styles => {thumb: '70x70>'}, default_url: '/assets/missing.png'

	# Checks for attachment content type & size
	validates_attachment_size :docu, :less_than => 5.megabytes
	validates_attachment :docu, 
	:content_type => {
		:content_type => %w(image/jpeg image/jpg image/png)
	}
end
