include ActionView::Helpers::DateHelper

class Ticket < ApplicationRecord
	default_scope { order(created_at: :desc) }

	belongs_to :customer, foreign_key: :customer_id, class_name: 'User'
	belongs_to :support, foreign_key: :support_id, class_name: 'User', optional: true
	
	has_many :ticket_attachments, dependent: :destroy
	has_many :comments, dependent: :destroy

	validates_inclusion_of :status, in: 0..2, if: 'status.present?'
	validates :customer_id, :subject, :description, presence: true

	STATUSES = ['open', 'in-progess', 'closed']

	scope :open, -> {where(status: 0)}
	scope :in_progress, -> {where(status: 1)}
	scope :closed, -> {where(status: 2)}
	scope :closed_ticket_last_month, -> (user_id) { where("tickets.created_at > ? AND support_id = ? AND status = 2", 1.month.ago, user_id) }

	def get_status
		STATUSES[status]
	end

	def take_responsibility(support)
		update_attributes support: support, status: 2
	end

	def close_ticket
		update_attributes status: 3
	end

	def rendered_attachments
		res = []
		ticket_attachments.each { |attachment| res << { thumb: attachment.docu.url(:thumb), original: attachment.docu.url(:original) } }
		res
	end

	class << self
		def get_ticket_comments_and_attachments(ticket_id, user)						
			res = []
			comments = nil
			cm_attachement = []

			ticket = find(ticket_id)
			
			if !user.is_admin?
				comments = ticket.comments.try(:active_comments) # shows ONLY comments w/ status true
			else
				comments = ticket.comments # shows all
			end

			unless comments.nil?
				comments.each do |comment|
					local_res = comment.as_json
					local_res['attachments'] = comment.rendered_attachments
					local_res['commenter'] = comment.user.try(:name).try(:capitalize)
					local_res['posted_at'] = time_ago_in_words(comment.created_at.to_time) + ' ago'
					res << local_res
				end
			end
			{comments: res}
		end

		#show
		def show_ticket_and_attachments(id)
			ticket = find(id)
			res = ticket.as_json
			res['attachments'] = ticket.rendered_attachments

			{
			 ticket: res, 
			 msg: 'Success', 
			 customer: ticket.customer.try(:name),
			 support: ticket.support.try(:name),
			 status: ticket.get_status
			}
		end
	end
end
