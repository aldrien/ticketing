module Ticketing::Exception
  # Forbidden
  # Should be handled by throwing 403 error
  class Forbidden < StandardError; end

  # Unauthorized
  # Should be handled by throwing 401 error
  class Unauthorized < StandardError; end

  # NotAllowed
  # Should be handled by throwing 405 error
  class NotAllowed < StandardError; end

  # BadRequest
  # Should be handled by throwing 400 error
  class BadRequest < StandardError; end

  # NotImplemented
  # Should be handled by throwing 501 error
  class NotImplemented < StandardError; end

  # TooManyUploads custom exception
  # Should be handled by throwing 422 error
  class TooManyUploads < StandardError; end
end