class CreateTicketAttachments < ActiveRecord::Migration[5.0]
  def up
    create_table :ticket_attachments do |t|
      t.integer :ticket_id
      
      t.timestamps
    end
    add_attachment :ticket_attachments, :docu
  end

  def down
  	drop_table :ticket_attachments
    remove_attachment :ticket_attachments, :docu
  end
end
