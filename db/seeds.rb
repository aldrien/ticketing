# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Default Users
puts 'Initialize creating default Users.....'
# Admin
@admin = User.create!(name: 'admin', email: 'admin@gmail.com', password: 'test123', password_confirmation: 'test123', role: 3)
# Customer
@customer = User.create!(name: 'customer', email: "customer@gmail.com", password: 'test123', password_confirmation: 'test123', role: 1)
# Support
@support = User.create!(name: 'support', email: "support@gmail.com", password: 'test123', password_confirmation: 'test123', role: 2)
puts 'Done creating default Users.....'

# Default Tickets
puts 'Initialize creating tickets.....'
# create closed tickets
(1..5).each do
	Ticket.create!(subject: Faker::Lorem.word, description: Faker::Lorem.sentence, support: @support , customer: @customer, status: 2, closed_at: Time.new)
end
# create in-progress tickets
(1..5).each do	
	Ticket.create!(subject: Faker::Lorem.word, description: Faker::Lorem.sentence, support: @support , customer: @customer, status: 1)
end
# create open tickets
(1..5).each do
	Ticket.create!(subject: Faker::Lorem.word, description: Faker::Lorem.sentence, support: nil, customer: @customer, status: 0)
end
puts 'Done creating tickets.....'


# Default Comments
puts 'Initialize creating comments.....'
comments = []
(1..3).each do |idx|
	comments << {ticket_id: idx, user: @support, message: Faker::Lorem.sentence}
end
Comment.create!(comments)
puts 'Done creating comments.....'

# Default TicketAttachment
puts 'Initialize creating Ticket Attachments.....'
TicketAttachment.create!(ticket_id: 1, docu: File.new("#{Rails.root}/public/fixtures/sample.png"))
puts 'Done creating Ticket Attachments.....'

# Default CommentAttachment
puts 'Initialize creating Comment Attachments.....'
CommentAttachment.create!(comment_id: 1, docu: File.new("#{Rails.root}/public/fixtures/sample.png"))
puts 'Done creating Comment Attachments.....'

puts 'Finish creating test data.'
puts "Please use ADMIN account: #{@admin.email} using password 'test123' "
puts "Please use CUSTOMER account: #{@customer.email} using password 'test123' "
puts "Please use SUPPORT account: #{@support.email} using password 'test123' "