Rails.application.routes.draw do
  namespace :api, defaults: { format: 'json' } do
    resources :users do
    	collection do
    		get 'success_login'
		  end
	  end

    resources :tickets do
      member do
        get :get_ticket_comments_and_attachments
      end
      collection do
        get :take_or_close_ticket
      end
    end

    resources :comments
  end
  
  get 'generate_closed_ticket_report' => 'dashboard#generate_closed_ticket_report'

  mount_devise_token_auth_for 'User', at: 'auth'

  root to: 'dashboard#index'
end
