if Rails.env.production?
	BASE_URL = 'enter_production_domain'
else
	BASE_URL = 'http://localhost:3000'
end