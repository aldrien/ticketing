require 'rails_helper'

RSpec.describe 'Comment and Authentication', :type => :request do
	include_examples 'login_user'
    
	before(:each) do
	  @customer = FactoryGirl.create(:user, name: Faker::Name.name, email: Faker::Internet.email, password: '123abc', password_confirmation: '123abc')
	  @ticket = FactoryGirl.create(:ticket, subject: 'sample', description: 'test', customer_id: @customer.id, status: 0)
	  @comment = FactoryGirl.create(:comment, ticket_id: @ticket.id, user_id: @customer.id, status: 1)
	  @comment_attachment = FactoryGirl.create(:comment_attachment, comment_id: @comment.id, docu: File.new("#{Rails.root}/public/fixtures/sample.png"))
	end

  ## Testing API
  it 'POST #create' do
    comment_params = {
    	user_id: user.id,
    	ticket_id: @ticket.id,
    	message: Faker::Lorem.sentence
	  }
	    
	  post api_comments_path(comment: comment_params), headers: @headers, xhr: true
	  expect(JSON.parse(response.body).size).to eq(1)
	  expect(JSON.parse(response.body)['comment']['message']).to eq(comment_params[:message])
	  expect(response).to have_http_status(:success)      
  end

  it 'PATCH #update' do
    patch "/api/comments/#{@comment.id}", params:{ comment: {message: 'new subject'} }, headers: @headers, xhr: true
    parsed_response = JSON.parse(response.body)
    expect(parsed_response['comment']['message']).to eq('new subject')
    expect(response).to have_http_status(:success)
  end

  it 'DELETE #destroy' do
    delete "/api/comments/#{@comment.id}", headers: @headers, xhr: true

    parsed_response = JSON.parse(response.body)
    expect(response).to have_http_status(:success)
  end
end
