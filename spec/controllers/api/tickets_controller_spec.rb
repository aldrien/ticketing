require 'rails_helper'
include ActionDispatch::TestProcess

RSpec.describe "Ticket and Authentication", :type => :request do
	include_examples 'login_user'

	before(:each) do
	  @customer = FactoryGirl.create(:user, name: Faker::Name.name, email: Faker::Internet.email, password: '123abc', password_confirmation: '123abc')
	  @ticket = FactoryGirl.create(:ticket, subject: 'sample', description: 'test', customer_id: @customer.id, status: 0)
	  @comment = FactoryGirl.create(:comment, ticket_id: @ticket.id, user_id: @customer.id, status: 1)
	  @comment_attachment = FactoryGirl.create(:comment_attachment, comment_id: @comment.id, docu: File.new("#{Rails.root}/public/fixtures/sample.png"))
	end


	## Testing API
  it 'GET #index Shows tickets' do
    get url_for({ controller: 'api/tickets', action: 'index', params: @headers }), xhr: true        
    parsed_response = JSON.parse(response.body)

    expect(parsed_response['open_tickets'][0]['subject']).to eq(@ticket.subject)
    expect(response).to have_http_status(:success)
  end

  it 'GET #show Show Single Ticket' do
    get "/api/tickets/#{@ticket.id}", params: @headers, xhr: true
    expect(response).to have_http_status(:success)
    expect(JSON.parse(response.body)['ticket']['subject']).to eq(@ticket.subject)
  end

  it 'GET #get_ticket_comments_and_attachments Shows ticket comments and comment_attachment' do
    get "/api/tickets/#{@ticket.id}/get_ticket_comments_and_attachments", params: @headers, xhr: true
    parsed_response = JSON.parse(response.body)
    expect(parsed_response['comments'][0]['message']).to eq(@comment.message)
    expect(response).to have_http_status(:success)
  end

  it 'POST #create Creates new ticket' do
    ticket_params = {
    	subject: Faker::Lorem.word,
    	description: Faker::Lorem.sentence,
    	customer_id: @customer.id
    }
    
    post api_tickets_path(ticket: ticket_params), headers: @headers, xhr: true
    expect(JSON.parse(response.body).size).to eq(1)
    expect(JSON.parse(response.body)['ticket']['subject']).to eq(ticket_params[:subject])
    expect(response).to have_http_status(:success)
  end

  it 'PATCH #update Update Ticket' do
    ticket_params = {
      subject: 'new subject',
      description: Faker::Lorem.sentence
    }

    patch "/api/tickets/#{@ticket.id}", params:{ ticket: ticket_params}, headers: @headers, xhr: true

    parsed_response = JSON.parse(response.body)
    expect(parsed_response['ticket']['subject']).to eq('new subject')
    expect(response).to have_http_status(:success)
  end

  it 'DELETE #destroy Delete Ticket' do
    delete "/api/tickets/#{@ticket.id}", headers: @headers, xhr: true
 	  expect(response).to have_http_status(:success)
  end
end
