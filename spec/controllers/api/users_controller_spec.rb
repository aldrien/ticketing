require 'rails_helper'

RSpec.describe 'Authentication', type: 'request' do
    # Api Test
    context 'Normal auth' do
      let(:user) { FactoryGirl.create(:user, password: 'test123', confirmed_at: Time.now) }
      it 'should able to login via api do' do
      	post '/auth/sign_in', params: {email: user.email, password: 'test123'}, xhr: true
        expect(response).to have_http_status(:success)
      end

      it 'should able to signup' do
        xhr :post, user_registration_path, { email: Faker::Internet.email, password: 'test123', password_confirmation: 'test123'  }
        expect(response).to have_http_status(:success)
      end

    end

end