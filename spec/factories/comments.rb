FactoryGirl.define do
  factory :comment do
    ticket_id 1
    user_id 1
    message Faker::Lorem.sentence
  end
end
