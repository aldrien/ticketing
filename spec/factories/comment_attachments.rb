FactoryGirl.define do
  factory :comment_attachment do
    comment_id 1
    docu Faker::File.file_name('foo/bar', 'test', 'doc')
  end
end
