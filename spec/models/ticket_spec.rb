require 'rails_helper'
include ActionDispatch::TestProcess

RSpec.describe Ticket, type: :model do
  # Test for requiring value for required fields
  %w(subject description customer_id).each do |field|
    context "validate presence of #{field}" do  	    
  	let(:ticket) {  FactoryGirl.build(:ticket, field.to_sym => nil) }
		let(:field) { field.to_sym }
        let(:msg) { "can't be blank" }
		include_examples 'has_validation_error'
    end
  end 

  # Test for validating ticket status value
  context 'validate inclusion of status' do
  	let(:ticket) {FactoryGirl.build(:ticket, status: 4)}
  	let(:field) {:status}
    let(:msg) { "is not included in the list" }
  	include_examples 'has_validation_error'
  end

  # Test for getting ticket and ticket_attachments
  context 'get ticket and ticket_attachments' do
    let(:init_ticket) { Ticket }
    let(:customer) { FactoryGirl.create(:user) }
    let(:ticket) { FactoryGirl.create(:ticket, customer: customer) }
    let(:ticket_attachment) { FactoryGirl.create(:ticket_attachment, ticket_id: ticket.id, docu: File.new("#{Rails.root}/public/fixtures/sample.png")) }

    it 'returns json response' do
      expect( init_ticket.show_ticket_and_attachments(ticket.id)[:msg]).to eq('Success')
    end
  end

end