require 'rails_helper'

RSpec.describe CommentAttachment, type: :model do
	context 'should validate content type' do
		include_examples 'validate_content_type', {model: :ticket_attachment}
	end
end
