require 'rails_helper'

RSpec.shared_examples 'validate_content_type' do |params|
  # Test for validating attachment content type
  let(:docu) {FactoryGirl.build(params[:model], docu: File.new("#{Rails.root}/public/favicon.ico"))}
  let(:error_msgs) { ["has contents that are not what they are reported to be", "is invalid", "content type is invalid"] }
  
  it 'returns error' do
    docu.valid?
      docu.errors[:docu].each do |error|
        expect(error_msgs).to include(error)
      end
  end
end

RSpec.shared_examples 'has_validation_error' do
# Method for checking error count/length
    it 'should validate' do
  		ticket.valid?	 
      expect(ticket.errors[field].first).to eq(msg)
  	end
end

RSpec.shared_examples 'login_user' do
  # For user authentication
  let(:user) { FactoryGirl.create(:user, password: 'test123', confirmed_at: Time.now) }

  before(:each) do
    post user_session_path, params: {email: user.email, password: 'test123'}, xhr: true
    @headers = {
      'access-token': response.headers['access-token'],
      'token-type':   'Bearer',
      'client':       response.headers['client'],
      'expiry':       response.headers['expiry'],
      'uid':          response.headers['uid']
    }
  end

  it 'should have access_token' do
    expect(JSON.parse(response.body).size).to eq(1)
    expect(response).to have_http_status(:success)
  end
end